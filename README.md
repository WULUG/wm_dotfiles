# Amey's dotfiles

Copy the stuff from folder amey and put them under: 
~/.config/

After copying, the path should look like: ~/.config/i3

Note: You will have to move the wall file inside i3 to ~/pictures if you want
same wallpaper as mine.

****
-- **Packages Required** --

I use rxvt-unicode as my terminal, but feel free to change it to whatever
inside the config file. 

-- Packages needed to make this work --

* i3 windows manager
* i3blocks
* Fonts: font-awesome powerline-fonts hack

Entirely optional but I have a sick console display manager called ly.

You can install it from:   https://github.com/cylgom/ly

**-- Screenshots --**

![dirty](https://gitlab.com/WULUG/wm_dotfiles/raw/master/amey/pics/2019-04-05-141952_1920x1080_scrot.png?inline=false)
![clean](https://gitlab.com/WULUG/wm_dotfiles/raw/master/amey/pics/2019-04-05-135641_1920x1080_scrot.png?inline=false)
![display_manager](https://user-images.githubusercontent.com/5473047/42466218-8cb53d3c-83ae-11e8-8e53-bae3669f959c.png)
